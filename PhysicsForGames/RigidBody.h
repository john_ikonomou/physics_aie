#pragma once
#include "PhysicsActor.h"

struct PhysicsInfo
{
	PhysicsInfo() { Drag = 1, /*RotationDrag = 1,*/ Elasticity = FLT_MAX; }
	PhysicsInfo(float a_Drag, 
				float a_RotationDrag, 
				float a_Elasticity)
	{ Drag = a_Drag, 
	  //RotationDrag = a_RotationDrag, 
	  Elasticity = a_Elasticity; }
	
	float Drag;
	//float RotationDrag;
	float Elasticity;
};

class RigidBody : public PhysicsActor
{
public:
	RigidBody() = delete;
	RigidBody(PhysicsActor::ShapeType eType);
	
	virtual void Update(float fDeltaTime) override;

	glm::vec3 GetAcceleration() const { return m_Acceleration; }
	void SetAcceleration(glm::vec3 A) { m_Acceleration = A; }

	glm::vec3 GetVelocity() const { return m_Velocity; }
	void SetVelocity(const glm::vec3 A) { m_Velocity = A; }

	glm::vec3 GetForces() const { return m_Forces; }
	void SetForces(glm::vec3 A) { m_Forces = A; }

	glm::vec3 GetMomentum() const { return m_Momentum; }
	void SetMomentum(glm::vec3 A) { m_Momentum = A; }

	PhysicsInfo GetPhysicsInfo() const { return m_pInfo; }
	void SetPhysicsInfo(PhysicsInfo A) { m_pInfo = A; }

	glm::vec3 GetEnergy() const { return m_Velocity * this->GetMass(); }

	void AddForce(glm::vec3 vForce, bool isImpulse = false);
	void AddForceToActor(glm::vec3 vForce, RigidBody* aActor, bool isImpulse = false);
	void ClearForces();

private:
	glm::vec3 m_Velocity;
	glm::vec3 m_Acceleration;
	glm::vec3 m_Forces;
	glm::vec3 m_Momentum;

	PhysicsInfo m_pInfo;

	
public:
	//Oiler Integration
	glm::vec3 SimulateIntegrationEuler(float Time, glm::vec3 iVel, glm::vec3 iPos);
};