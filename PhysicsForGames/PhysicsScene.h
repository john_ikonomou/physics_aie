#pragma once

#include "glm/glm.hpp"
#include "glm/ext.hpp"
#include <vector>

class PhysicsScene
{
public:
	PhysicsScene();
	~PhysicsScene();

	glm::vec3 GetGravity() const { return m_Gravity; }
	void SetGravity(glm::vec3 val) { m_Gravity = val; }

	float GetTimestep() const { return m_TimeStep; }
	void SetTimestep(float val) { m_TimeStep = val; }

	void AddActor(class PhysicsActor* pActor);
	void RemoveActor(class PhysicsActor* pActor);
	void RemoveAllActors();



	void Update(float DeltaTime);
	void DebugDraw() const;
	void CheckCollisions();

private:
	glm::vec3 m_Gravity;
	float m_TimeStep;

	std::vector<class PhysicsActor*> m_Actors;
	std::vector<class PhysicsActor*> m_Springs;

public:
	static bool Box2Box(PhysicsActor* A, PhysicsActor* B);
	static bool Box2Sphere(PhysicsActor* A, PhysicsActor* B);
	static bool Box2Plane(PhysicsActor* A, PhysicsActor* B);
	static bool S2P(PhysicsActor* A, PhysicsActor* B, PhysicsActor* C);
	static bool Box2StaticBox(PhysicsActor* A, PhysicsActor* B);
	static bool Box2StaticSphere(PhysicsActor* A, PhysicsActor* B);

	static bool Sphere2Box(PhysicsActor* A, PhysicsActor* B);
	static bool Sphere2Sphere(PhysicsActor* A, PhysicsActor* B);
	static bool Sphere2Plane(PhysicsActor* A, PhysicsActor* B);
	static bool Sphere2StaticBox(PhysicsActor* A, PhysicsActor* B);
	static bool Sphere2StaticSphere(PhysicsActor* A, PhysicsActor* B);

	static bool Plane2Box(PhysicsActor* A, PhysicsActor* B);
	static bool Plane2Sphere(PhysicsActor* A, PhysicsActor* B);
	static bool Plane2Plane(PhysicsActor* A, PhysicsActor* B);
	static bool Plane2StaticBox(PhysicsActor* A, PhysicsActor* B);
	static bool Plane2StaticSphere(PhysicsActor* A, PhysicsActor* B);

	static bool StaticBox2Box(PhysicsActor* A, PhysicsActor* B);
	static bool StaticBox2Sphere(PhysicsActor* A, PhysicsActor* B);
	static bool StaticBox2Plane(PhysicsActor* A, PhysicsActor* B);
	static bool StaticBox2StaticBox(PhysicsActor* A, PhysicsActor* B);
	static bool StaticBox2StaticSphere(PhysicsActor* A, PhysicsActor* B);

	static bool StaticSphere2Box(PhysicsActor* A, PhysicsActor* B);
	static bool StaticSphere2Sphere(PhysicsActor* A, PhysicsActor* B);
	static bool StaticSphere2Plane(PhysicsActor* A, PhysicsActor* B);
	static bool StaticSphere2StaticBox(PhysicsActor* A, PhysicsActor* B);
	static bool StaticSphere2StaticSphere(PhysicsActor* A, PhysicsActor* B);
};

