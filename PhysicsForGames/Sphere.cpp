#include "Sphere.h"

Sphere::Sphere() : RigidBody(ShapeType::DynamicSphere)
{
	SetRadius(0.0f);
}

Sphere::Sphere(glm::vec3 Position, float Radius, float Mass, glm::vec4 Colour) : RigidBody(ShapeType::DynamicSphere)
{
	SetPosition(Position);
	SetRadius(Radius);
	SetMass(Mass);
	SetDebugColour(Colour);
}

void Sphere::DebugDraw() const
{
	Gizmos::addSphereFilled(GetPosition(), m_Radius, 8, 8, GetDebugColour());
}

StaticSphere::StaticSphere() : StaticObject(ShapeType::StaticSphere)
{
	SetRadius(0.0f);
}

StaticSphere::StaticSphere(glm::vec3 Position, float Radius, glm::vec4 Colour) : StaticObject(ShapeType::StaticSphere) 
{
	SetPosition(Position);
	SetRadius(Radius);
	SetDebugColour(Colour);
}

void StaticSphere::DebugDraw() const
{
	Gizmos::addSphere(GetPosition(), m_Radius, 8, 8, GetDebugColour());
}