#pragma once
#include "glm/glm.hpp"
#include "glm/ext.hpp"

#define NUMBERSHAPES 5
class PhysicsActor
{
public:
    enum ShapeType
    {
        Spring = -1,
        DynamicBox = 0,
        DynamicSphere = 1,
        StaticPlane = 2,
        StaticBox = 3,
        StaticSphere = 4
    };

    PhysicsActor() = delete;
    PhysicsActor(ShapeType a_ShapeID);

    virtual void Update(float DeltaTime) = 0;
    virtual void DebugDraw() const = 0; //Const because no member variables need to be affected


#pragma region GetterSetters
    float GetMass() const { return m_Mass; }
    //Sets the Mass in KG's
    void SetMass(const float A) { m_Mass = A; }

    glm::vec3 GetPosition() const { return m_Position; }
    void SetPosition(const glm::vec3 A) { m_Position = A; }

    class PhysicsScene* GetOwningScene() const { return m_OwningScene; }
    void SetOwningScene(PhysicsScene* A) { m_OwningScene = A; }

    glm::vec4 GetDebugColour() const { return m_DebugColour; }
    void SetDebugColour(const glm::vec4 A) { m_DebugColour = A; }

    PhysicsActor::ShapeType GetShapeID() const { return m_ShapeType; }
#pragma endregion
private:
    float m_Mass;
    glm::vec3 m_Position;

    ShapeType m_ShapeType;

    class PhysicsScene* m_OwningScene;

    glm::vec4 m_DebugColour;
};

