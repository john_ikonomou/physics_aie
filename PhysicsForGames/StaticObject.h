#pragma once
#include "PhysicsActor.h"
class StaticObject : public PhysicsActor
{
public:
	StaticObject() = delete;
	StaticObject(PhysicsActor::ShapeType eType);

	virtual void Update(float DeltaTime) override;
};

