#ifndef PHYSICS_PROGRAMMING_H
#define PHYSICS_PROGRAMMING_H

#include "Application.h"
#include "Camera.h"
#include "Render.h"

#include "Sphere.h"
#include "Plane.h"
#include "Box.h"
#include "Spring.h"
#include "Ragdoll.h"
#include "ParticleFluidEmitter.h"
#include "CollisionCallback.h"

#include "PhysicsScene.h"

#include <PxPhysicsAPI.h>
#include <PxScene.h>

using namespace physx;

struct FilterGroup
{
	enum Enum
	{
		ePlayer = (1 << 0),
		ePlatform = (1 << 1),
		eGround = (1 << 2)
	};
};

class MyControllerHitReport : public PxUserControllerHitReport
{
public:
	//overload the onShapeHit function
	virtual void onShapeHit(const PxControllerShapeHit &hit);
	//other collision functions which we must overload 
	//these handle collisionwith other controllers and hitting obstacles
	virtual void onControllerHit(const PxControllersHit &hit) 
	{

	};

	//Called when current controller hits another controller. More...
	virtual void onObstacleHit(const PxControllerObstacleHit &hit) 
	{
	
	};

	//Called when current controller hits a user-defined obstacl
	MyControllerHitReport() :PxUserControllerHitReport() 
	{
	
	};
	PxVec3 getPlayerContactNormal() 
	{ 
		return _playerContactNormal; 
	};
	
	void clearPlayerContactNormal()
	{
		_playerContactNormal = PxVec3(0, 0, 0);
	};
	PxVec3 _playerContactNormal;
};

class Physics : public Application
{
public:
	virtual bool startup();
	virtual void shutdown();
    virtual bool update();
    virtual void draw();

	void SetupPhysX();
	PxScene* createDefaultPhysxScene();
	void renderGizmos(PxScene* physics_scene);
	void setupIntroductionToPhysX();

	void calculateSimulation(std::vector<glm::vec3>* Lines, float Time, RigidBody* rBody, bool Calculate = true);
	void renderSimulationLines(std::vector<glm::vec3> Lines);

    Renderer* m_renderer;
	std::vector<PhysicsActor*> m_Objects;
	Plane* pPlane;

	StaticBox* Box1;
	Sphere* Sphere1;
	Spring* Spring1;

	unsigned int FrameCounter;
	unsigned int SceneObjects;

	PhysicsScene* pScene;
    FlyCamera m_camera;
    float m_delta_time;

	std::vector<glm::vec3> Simulation;
	bool CalcBool = true;

	float TotalTime = 0;

	//PHYSX STUFF
	static PxFilterFlags myFilterShader(PxFilterObjectAttributes attributes0, PxFilterData filterData0, PxFilterObjectAttributes attributes1, PxFilterData filterData1,PxPairFlags& pairFlags, const void* constantBlock, PxU32 constantBlockSize);
	static void Physics::setupFiltering(PxRigidActor* actor, PxU32 filterGroup, PxU32 filterMask);
	static void Physics::setShapeAsTrigger(PxRigidActor* actorIn);

	PxFoundation* m_physics_foundation;
	PxPhysics* m_physics;
	PxScene* m_physics_scene;

	PxDefaultErrorCallback m_default_error_callback;
	PxDefaultAllocator m_default_allocator;
	PxSimulationFilterShader m_default_filter_shader;


	PxMaterial* m_physics_material;
	PxMaterial* m_box_material;
	PxCooking* m_physics_cooker;
	PxControllerManager* m_controller_manager;

	static bool s_isFluidActive;

	ParticleFluidEmitter* m_particleEmitter;
};


#endif PHYSICS_PROGRAMMING_H
