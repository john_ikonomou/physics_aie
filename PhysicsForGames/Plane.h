#pragma once
#include "StaticObject.h"

class Plane : public StaticObject
{
public:
	Plane();

	virtual void DebugDraw() const override;

	glm::vec3 GetDirection() const { return glm::normalize(m_Direction); }
	void SetDirection(glm::vec3 val) { m_Direction = glm::normalize(val); }

	float GetDistance() const { return m_Distance; }
	void SetDistance(float val) { m_Distance = val; }


private:
	glm::vec3 m_Direction;
	float	  m_Distance;
};

