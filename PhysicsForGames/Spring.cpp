#include "Spring.h"

#include "Gizmos.h"
#include "RigidBody.h"
#include "StaticObject.h"

Spring::Spring() : PhysicsActor(PhysicsActor::ShapeType::Spring)
{
    Start = nullptr;
    End = nullptr;
}

Spring::Spring(float a_Damping, float a_SpringCo) : PhysicsActor(PhysicsActor::ShapeType::Spring)
{
    Start = nullptr;
    End = nullptr;
    m_Damping = a_Damping;
    m_SpringConstant = a_SpringCo;
    RestLength = 0; //glm::length(Start->GetPosition() - End->GetPosition());
}

Spring::~Spring()
{

}

void Spring::Attach(PhysicsActor* A, PhysicsActor* B)
{
    Start = A;
    End = B;
    RestLength = glm::length(Start->GetPosition() - End->GetPosition());
}

void Spring::Update(float deltaTime)
{
    glm::vec3   Direction = End->GetPosition() - Start->GetPosition();
    float       Displacement = glm::length(Start->GetPosition() - End->GetPosition()) - RestLength;
    float       Force = -GetSpringConstant() * Displacement;

    RigidBody* StartRigid = nullptr;
    RigidBody* EndRigid = nullptr;
    //check if enum lies in the "dynamic" range
    if (Start->GetShapeID() >= PhysicsActor::DynamicBox &&
        Start->GetShapeID() <= PhysicsActor::DynamicSphere)
    {
        StartRigid = (RigidBody*)Start;
    }
    if (End->GetShapeID() >= PhysicsActor::DynamicBox &&
        End->GetShapeID() <= PhysicsActor::DynamicSphere)
    {
        EndRigid = (RigidBody*)End;
    }

    glm::vec3 RelativeVelocity = (StartRigid != nullptr) ? StartRigid->GetVelocity() : glm::vec3(0);
    RelativeVelocity -= (EndRigid != nullptr) ? EndRigid->GetVelocity() : glm::vec3(0);


    if (StartRigid != nullptr)
    {
        StartRigid->AddForce(-Direction * Force - (GetDamping() * -RelativeVelocity));
    }
    if (EndRigid != nullptr)
    {
        EndRigid->AddForce(Direction * Force - (GetDamping() * -RelativeVelocity));
    }
}

void Spring::DebugDraw() const
{
    Gizmos::addLine(Start->GetPosition(), End->GetPosition(), GetDebugColour());
}
