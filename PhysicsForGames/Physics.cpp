#include "Physics.h"

#include "gl_core_4_4.h"
#include "GLFW/glfw3.h"
#include "Gizmos.h"

#include "glm/ext.hpp"
#include "glm/gtc/quaternion.hpp"

#include <iostream>
#include <thread>

bool Physics::s_isFluidActive = false;

#define Assert(val) if (val){}else{ *((char*)0) = 0;}
#define ArrayCount(val) (sizeof(val)/sizeof(val[0]))

PxControllerManager* gCharacterManager;
PxController* gPlayerController;
//set up some variables to control our player with
float _characterYVelocity = 0; //initialize character velocity
float _characterRotation = 0; //and rotation
float _playerGravity = -0.5f; //set up the player gravity
MyControllerHitReport* myHitReport;
PxSimulationEventCallback* myCollisionCallBack;

bool Physics::startup()
{
    if (Application::startup() == false)
    {
        return false;
    }

	srand((UINT)(time(NULL)));
    glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    Gizmos::create();
	FrameCounter = 0;

    m_camera = FlyCamera(1680 / 1050.0f, 10.0f);
    m_camera.setLookAt(vec3(60, 40, 5), vec3(0, 10, 0), vec3(0, 1, 0));
    m_camera.sensitivity = 3;

	// ===| CUSTOM API |===
	pScene = new PhysicsScene();
	pScene->SetTimestep(0.05f);
	pScene->SetGravity(vec3(0, -9.80, 0));

	pPlane = new Plane();
	pPlane->SetDirection(glm::vec3(0.00, 1, 0));
	pPlane->SetDistance(-20);
	pPlane->SetDebugColour(glm::vec4(1, 0, 1, 0.4f));
	pScene->AddActor(pPlane);

	Sphere1 = new Sphere(vec3(0, 15, 50), 2.5f, 10.0f, vec4(1, 0, 0, 1));
	pScene->AddActor(Sphere1);
	Box1 = new StaticBox(vec3(0, 30, 50), vec3(0.5, 0.5, 0.5),  vec4(0, 1, 1, 1));
	pScene->AddActor(Box1);
	Spring1 = new Spring(0.26f, 0.475f);
	Spring1->Attach(Box1, Sphere1);
	pScene->AddActor(Spring1);
	SceneObjects = 0;
	

	m_renderer = new Renderer();

	//PHYSX STUFF
	SetupPhysX();
	setupIntroductionToPhysX();
	
    return true;
}

void Physics::shutdown()
{
	m_physics_scene->release();
	m_physics_foundation->release();
	m_physics->release();

	delete m_renderer;
    Gizmos::destroy();
    Application::shutdown();
}

bool Physics::update()
{
	system("cls");
    if (Application::update() == false)
    {
        return false;
    }

    Gizmos::clear();



    float dt = (float)glfwGetTime();
    m_delta_time = dt;
	std::cout << "FPS: " << (1000 / m_delta_time) / 1000 << std::endl;
    glfwSetTime(0.0);
	//pScene->SetTimestep(dt);
	TotalTime += dt;
   
	//DRAW THE GRID
	vec4 white(0);
    vec4 black(0, 1, 0, 1);

    for (int i = 0; i <= 20; ++i)
    {
        Gizmos::addLine(vec3(-10 + i, -0.01, -10), vec3(-10 + i, -0.01, 10),
            i == 10 ? white : black);
        Gizmos::addLine(vec3(-10, -0.01, -10 + i), vec3(10, -0.01, -10 + i),
            i == 10 ? white : black);
    }

	//CUSTOM PHYSICS API
	if (FrameCounter % 1 == 0 && SceneObjects < 250)
	{
		glm::vec4 dCol = glm::vec4((float)((rand() % 100) / 100.f),
								   (float)((rand() % 100) / 100.f),
								   (float)((rand() % 100) / 100.f), 1);
		glm::vec3 Pos = glm::vec3(-5 + rand() % 10, 50, -5 + rand() % 10);
		glm::vec3 Force = glm::vec3(rand() % 5, rand() % 5, rand() % 5);

		PhysicsInfo pA = PhysicsInfo();
		pA.Drag = 0.997f;
		pA.Elasticity = 0.95;
		//pA.RotationDrag = 1;

		++SceneObjects;
		FrameCounter = 0;
		switch  (rand() % 2)
		{
		case 0:
			{
				Sphere* A = new Sphere();
				A->SetRadius(1);
				A->SetPosition(Pos);
				A->SetMass(0.125f);
				A->SetDebugColour(dCol);

				A->SetPhysicsInfo(pA);
				A->AddForce(Force);
				pScene->AddActor(A);
				m_Objects.push_back(A);
				break;
			}
		case 1:
			{
				Box* B = new Box();
				B->SetExtents(glm::vec3(1, 1, 1));
				B->SetPosition(Pos);
				B->SetMass(0.125f);
				B->SetDebugColour(dCol);

				B->SetPhysicsInfo(pA);
				B->AddForce(Force);
				pScene->AddActor(B);
				m_Objects.push_back(B);

				break;
			}
		}
	}

	pScene->Update(pScene->GetTimestep() / 2);
	pScene->Update(pScene->GetTimestep() / 2);

	GLFWwindow* curr_window = glfwGetCurrentContext();

	if (glfwGetKey(curr_window, GLFW_KEY_F) == GLFW_PRESS)
	{		
		//Shoot a ball
		//=============
		vec3 cam_pos = m_camera.world[3].xyz();
		vec3 box_vel = -m_camera.world[2].xyz() * 20.0f;
		PxTransform box_transform(PxVec3(cam_pos.x, cam_pos.y, cam_pos.z));

		//Geo
		PxSphereGeometry sphere(0.5f);
		//Density
		float Density = 100;

		PxRigidDynamic* newActor = PxCreateDynamic(*m_physics, box_transform, sphere, *m_physics_material, Density);

		float MuzzleSpeed = 50;

		glm::vec3 direction(-m_camera.world[2]);
		physx::PxVec3 velocity = physx::PxVec3(direction.x, direction.y, direction.z) * MuzzleSpeed;
		newActor->setLinearVelocity(velocity, true);
		m_physics_scene->addActor(*newActor);
		//============
	}

	bool onGround; //set to true if we are on the ground
	float movementSpeed = 10.0f; //forward and back movement speed
	float rotationSpeed = 1.0f; //turn speed

	//check if we have a contact normal. if y is greater than .3 we assume this is solid ground. 
	//This is a rather primitive way to do this. Can you do better ?
	if (myHitReport->getPlayerContactNormal().y > 0.3f)
	{
		_characterYVelocity = -0.1f;
		onGround = true;
	}
	else
	{
		_characterYVelocity += _playerGravity * dt;
		onGround = false;
	}

	myHitReport->clearPlayerContactNormal();
	const PxVec3 up(0, 1, 0);

	//scan the keys and set up our intended velocity based on a global transform
	PxVec3 velocity(0, _characterYVelocity, 0);

	if (glfwGetKey(curr_window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		velocity.x -= movementSpeed * dt;
	}

	if (glfwGetKey(curr_window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		velocity.x += movementSpeed * dt;
	}

	if (glfwGetKey(curr_window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		velocity.z += movementSpeed * dt;
	}

	if (glfwGetKey(curr_window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		velocity.z -= movementSpeed * dt;
	}

	//To do.. add code to control z movement and jumping
	float minDistance = 0.001f;
	PxControllerFilters filter;
	//make controls relative to player facing
	PxQuat rotation(_characterRotation, PxVec3(0, 1, 0));

	//move the controller
	gPlayerController->move(rotation.rotate(velocity), minDistance, dt, filter);

	if (m_particleEmitter)
	{
		//TODO: Fix this:

		if (s_isFluidActive)
		{
			m_particleEmitter->update(dt);
			//render all our particles
			m_particleEmitter->renderParticles();
		}
	}

	//PHYSX SCENE
	if (dt > 0)
	{
		m_physics_scene->simulate(dt > 0.033f ? 0.033f : dt);
		while (m_physics_scene->fetchResults() == false);
	}

	renderGizmos(m_physics_scene);

    m_camera.update(1.0f / 60.0f);

	++FrameCounter;
    return true;
}

void Physics::draw()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_CULL_FACE);
	pScene->DebugDraw();
	//renderSimulationLines(Simulation);

    Gizmos::draw(m_camera.proj, m_camera.view);

    m_renderer->RenderAndClear(m_camera.view_proj);

    glfwSwapBuffers(m_window);
    glfwPollEvents();
}

//PhysX Stuff
void Physics::SetupPhysX()
{
	m_default_filter_shader = PxDefaultSimulationFilterShader;
	m_physics_foundation = PxCreateFoundation(PX_PHYSICS_VERSION, m_default_allocator, m_default_error_callback);

	m_physics = PxCreatePhysics(PX_PHYSICS_VERSION, *m_physics_foundation, PxTolerancesScale());

	PxInitExtensions(*m_physics);
	m_physics_material = m_physics->createMaterial(1, 1, 0);
	m_physics_cooker = PxCreateCooking(PX_PHYSICS_VERSION, *m_physics_foundation, PxCookingParams(PxTolerancesScale()));
}

PxScene * Physics::createDefaultPhysxScene()
{
	PxSceneDesc scene_desc(m_physics->getTolerancesScale());

	scene_desc.gravity = PxVec3(0, -9.807f, 0);
	scene_desc.filterShader = myFilterShader;

	UINT Threads = std::thread::hardware_concurrency();
	scene_desc.cpuDispatcher = PxDefaultCpuDispatcherCreate(Threads);

	PxScene* Result = m_physics->createScene(scene_desc);

	return Result;
}

void AddWidget(PxShape* shape, PxRigidActor* actor, vec4 geo_color)
{
    PxTransform full_transform = PxShapeExt::getGlobalPose(*shape, *actor);
    vec3 actor_position(full_transform.p.x, full_transform.p.y, full_transform.p.z);
    glm::quat actor_rotation(full_transform.q.w,
        full_transform.q.x,
        full_transform.q.y,
        full_transform.q.z);
    glm::mat4 rot(actor_rotation);

    mat4 rotate_matrix = glm::rotate(10.f, glm::vec3(7, 7, 7));

    PxGeometryType::Enum geo_type = shape->getGeometryType();

    switch (geo_type)
    {
    case (PxGeometryType::eBOX) :
    {
        PxBoxGeometry geo;
        shape->getBoxGeometry(geo);
        vec3 extents(geo.halfExtents.x, geo.halfExtents.y, geo.halfExtents.z);
        Gizmos::addAABBFilled(actor_position, extents, geo_color, &rot);
    } break;
    case (PxGeometryType::eCAPSULE) :
    {
        PxCapsuleGeometry geo;
        shape->getCapsuleGeometry(geo);
        Gizmos::addCapsule(actor_position, geo.halfHeight * 2, geo.radius, 16, 16, geo_color, &rot);
    } break;
    case (PxGeometryType::eSPHERE) :
    {
        PxSphereGeometry geo;
        shape->getSphereGeometry(geo);
        Gizmos::addSphereFilled(actor_position, geo.radius, 16, 16, geo_color, &rot);
    } break;
    case (PxGeometryType::ePLANE) :
    {

    } break;
    }
}
void Physics::renderGizmos(PxScene* physics_scene)
{
    PxActorTypeFlags desiredTypes = PxActorTypeFlag::eRIGID_STATIC | PxActorTypeFlag::eRIGID_DYNAMIC;
    PxU32 actor_count = physics_scene->getNbActors(desiredTypes);
    PxActor** actor_list = new PxActor*[actor_count];
	physics_scene->getActors(desiredTypes, actor_list, actor_count);
    
    vec4 geo_color(1, 0, 0.8, 1);
	vec4 geo_color2(0, .85f, 0.0, 1);


    for (int actor_index = 0;
        actor_index < (int)actor_count;
        ++actor_index)
    {
        PxActor* curr_actor = actor_list[actor_index];
        if (curr_actor->isRigidActor())
        {
            PxRigidActor* rigid_actor = (PxRigidActor*)curr_actor;
            PxU32 shape_count = rigid_actor->getNbShapes();
            PxShape** shapes = new PxShape*[shape_count];
            rigid_actor->getShapes(shapes, shape_count);

            for (int shape_index = 0;
                shape_index < (int)shape_count;
                ++shape_index)
            {
                PxShape* curr_shape = shapes[shape_index];
                AddWidget(curr_shape, rigid_actor, geo_color);
            }

            delete[]shapes;
        }
    }

    delete[] actor_list;

    int articulation_count = physics_scene->getNbArticulations();

    for (int a = 0; a < articulation_count; ++a)
    {
        PxArticulation* articulation;
		physics_scene->getArticulations(&articulation, 1, a);

        int link_count = articulation->getNbLinks();

        PxArticulationLink** links = new PxArticulationLink*[link_count];
        articulation->getLinks(links, link_count);

        for (int l = 0; l < link_count; ++l)
        {
            PxArticulationLink* link = links[l];
            int shape_count = link->getNbShapes();

            for (int s = 0; s < shape_count; ++s)
            {
                PxShape* shape;
                link->getShapes(&shape, 1, s);
                AddWidget(shape, link, geo_color2);
            }
        }
        delete[] links;
    }
}

void Physics::setupIntroductionToPhysX()
{
	m_physics_scene = createDefaultPhysxScene();

	//Add a plane
	PxTransform pose = PxTransform(PxVec3(0.0f, 0, 0.0f), PxQuat(PxHalfPi*1.0f, PxVec3(0.0f, 0.0f, 1.0f)));
	PxRigidStatic* Plane = PxCreateStatic(*m_physics, pose, PxPlaneGeometry(), *m_physics_material);
	//Add it to the PhysX Scene
	m_physics_scene->addActor(*Plane);

	//Add some boxes
	PxBoxGeometry side1(4.5, 1, .5);
	PxBoxGeometry side2(.5, 1, 4.5);

	pose = PxTransform(PxVec3(0.0f, 0.5, 4.0f));
	PxRigidStatic* box = PxCreateStatic(*m_physics, pose, side1, *m_physics_material);
	m_physics_scene->addActor(*box);

	pose = PxTransform(PxVec3(0.0f, 0.5, -4.0f));
	box = PxCreateStatic(*m_physics, pose, side1, *m_physics_material);
	m_physics_scene->addActor(*box);

	pose = PxTransform(PxVec3(4.0f, 0.5, 0));
	box = PxCreateStatic(*m_physics, pose, side2, *m_physics_material);
	m_physics_scene->addActor(*box);

	pose = PxTransform(PxVec3(-4.0f, 0.5, 0));
	box = PxCreateStatic(*m_physics, pose, side2, *m_physics_material);
	m_physics_scene->addActor(*box);

	pose = PxTransform(PxVec3(-8.0f, 0.5, 0));
	PxRigidStatic* Trigbox = PxCreateStatic(*m_physics, pose, side2, *m_physics_material);
	setupFiltering(Trigbox, FilterGroup::ePlatform, FilterGroup::ePlayer);
	setShapeAsTrigger(Trigbox);
	Trigbox->setName("TriggerBox");
	m_physics_scene->addActor(*Trigbox);


	//Add A Ragdoll
	RagdollNode* ragdollData[] =
	{
		new RagdollNode(PxQuat(PxPi / 2.0f,		   Z_AXIS), NO_PARENT,		 1, 3,    1,  1, "lower spine"),
		new RagdollNode(PxQuat(PxPi,			   Z_AXIS), LOWER_SPINE,	 1, 1,   -1,  1, "left pelvis"),
		new RagdollNode(PxQuat(0,				   Z_AXIS), LOWER_SPINE,	 1, 1,	 -1,  1, "right pelvis"),
		new RagdollNode(PxQuat(PxPi / 2.0f + 0.2f, Z_AXIS),	LEFT_PELVIS,	 5, 2,	 -1,  1, "L upper leg"),
		new RagdollNode(PxQuat(PxPi / 2.0f - 0.2f, Z_AXIS),	RIGHT_PELVIS,    5, 2,	 -1,  1, "R upper leg"),
		new RagdollNode(PxQuat(PxPi / 2.0f + 0.2f, Z_AXIS),	LEFT_UPPER_LEG,  5, 1.75,-1,  1, "L lower leg"),
		new RagdollNode(PxQuat(PxPi / 2.0f - 0.2f, Z_AXIS),	RIGHT_UPPER_LEG, 5, 1.75,-1,  1, "R lowerleg"),
		new RagdollNode(PxQuat(PxPi / 2.0f,		   Z_AXIS), LOWER_SPINE,     1, 3,    1, -1, "upper spine"),
		new RagdollNode(PxQuat(PxPi,			   Z_AXIS), UPPER_SPINE,     1, 1.5,  1,  1, "left clavicle"),
		new RagdollNode(PxQuat(0,				   Z_AXIS), UPPER_SPINE,     1, 1.5,  1,  1, "right clavicle"),
		new RagdollNode(PxQuat(PxPi / 2.0f,		   Z_AXIS), UPPER_SPINE,     1, 1,    1, -1, "neck"),
		new RagdollNode(PxQuat(PxPi / 2.0f,		   Z_AXIS), NECK,		     1, 3,    1, -1, "HEAD"),
		new RagdollNode(PxQuat(PxPi - .3,		   Z_AXIS), LEFT_CLAVICLE,   3, 1.5, -1,  1, "left upper arm"),
		new RagdollNode(PxQuat(0.3,				   Z_AXIS), RIGHT_CLAVICLE,  3, 1.5, -1,  1, "right upper arm"),
		new RagdollNode(PxQuat(PxPi - .3,		   Z_AXIS), LEFT_UPPER_ARM,  3, 1,	 -1,  1, "left lower arm"),
		new RagdollNode(PxQuat(0.3,				   Z_AXIS), RIGHT_UPPER_ARM, 3, 1,	 -1,  1, "right lower arm"),
		NULL
	};
	PxArticulation* ragDollArticulation;
	ragDollArticulation = Ragdoll::MakeRagdoll(m_physics, ragdollData, PxTransform(PxVec3(0, 10, 0)), .1f, m_physics_material);
	//Add to scene
	m_physics_scene->addArticulation(*ragDollArticulation);

	//Add a Character
	myHitReport = new MyControllerHitReport();
	gCharacterManager = PxCreateControllerManager(*m_physics_scene);

	//describe our controller...
	PxCapsuleControllerDesc desc;
	desc.height = 1.6f;
	desc.radius = 0.4f;
	desc.position.set(0, 0, 0);
	desc.material = m_physics_material;
	desc.reportCallback = myHitReport; //connect it to our collision detection routine
	desc.density = 10;

	gPlayerController = gCharacterManager->createController(desc); //create the layer controller
	gPlayerController->setPosition(PxExtendedVec3(0,0,10));
	gPlayerController->getActor()->setName("Player");

	setupFiltering(gPlayerController->getActor(), FilterGroup::ePlayer, FilterGroup::eGround | FilterGroup::ePlatform);

	myHitReport->clearPlayerContactNormal(); //initialize the contact normal (what we are in contact with)
	m_physics_scene->addActor(*gPlayerController->getActor());

	PxParticleFluid* pFluid;

	//Create particle system
	//Set immutable properties
	PxU32 maxParticles = 2500;
	bool perParticleRestOffset = false;
	pFluid = m_physics->createParticleFluid(maxParticles, perParticleRestOffset);

	pFluid->setRestParticleDistance(.3f);
	pFluid->setDynamicFriction(0.1);
	pFluid->setStaticFriction(0.1);
	pFluid->setDamping(0.1);
	pFluid->setParticleMass(.1);
	pFluid->setRestitution(0);
	//pFluid->setParticleReadDataFlag(PxParticleReadDataFlag::eDENSITY_BUFFER /*, true  */);
	pFluid->setParticleBaseFlag(PxParticleBaseFlag::eCOLLISION_TWOWAY, true);
	pFluid->setStiffness(100);


	if (pFluid)
	{
		m_physics_scene->addActor(*pFluid);
		m_particleEmitter = new ParticleFluidEmitter(maxParticles, PxVec3(0, 10, 0), pFluid, .01);
		m_particleEmitter->setStartVelocityRange(-0.001f, -250.0f, -0.001f, 0.001f, -250.0f, 0.001f);
	}

	myCollisionCallBack = new CollisionCallback();
	m_physics_scene->setSimulationEventCallback(myCollisionCallBack);
}



//Integration Debug
void Physics::calculateSimulation(std::vector<glm::vec3>* Lines, float Time, RigidBody* rBody, bool Calculate)
{
	if (Calculate)
	{
		Lines->clear();
		for (int i = 0; i < Time * 8; ++i)
		{
			float sTime = ((float)i / 8.0f);
			Lines->push_back(rBody->SimulateIntegrationEuler(sTime, rBody->GetVelocity(), rBody->GetPosition()));
		}
	}
}
void Physics::renderSimulationLines(std::vector<glm::vec3> Lines)
{
	for (int i = 1; i < Simulation.size(); ++i)
	{
		float Colour = (1.0f / (float)Simulation.size());
		Gizmos::addLine(Simulation[i], Simulation[i - 1], glm::vec4(0, Colour * i,0, 1));
	}
}

void MyControllerHitReport::onShapeHit(const PxControllerShapeHit & hit)
{
	//gets a reference to a structure which tells us what has been hit and where
	//get the acter from the shape we hit
	PxRigidActor* actor = hit.shape->getActor();

	//get the normal of the thing we hit and store it so the player controller can respond correctly
	_playerContactNormal = hit.worldNormal;

	//try to cast to a dynamic actor
	PxRigidDynamic* myActor = actor->is<PxRigidDynamic>();

	if (myActor)
	{
		//this is where we can apply forces to things we hit
	}
}

PxFilterFlags Physics::myFilterShader(PxFilterObjectAttributes attributes0, PxFilterData
	filterData0,
	PxFilterObjectAttributes attributes1, PxFilterData filterData1,
	PxPairFlags& pairFlags, const void* constantBlock, PxU32 constantBlockSize)
{
	// let triggers through
	if (PxFilterObjectIsTrigger(attributes0) ||
		PxFilterObjectIsTrigger(attributes1))
	{
		pairFlags = PxPairFlag::eTRIGGER_DEFAULT;
		return PxFilterFlag::eDEFAULT;
	}

	// generate contacts for all that were not filtered above
	pairFlags = PxPairFlag::eCONTACT_DEFAULT;
	// trigger the contact callback for pairs (A,B) where
	// the filtermask of A contains the ID of B and vice versa.
	if ((filterData0.word0 & filterData1.word1) &&
		(filterData1.word0 & filterData0.word1))
		pairFlags |= PxPairFlag::eNOTIFY_TOUCH_FOUND |
		PxPairFlag::eNOTIFY_TOUCH_LOST;

	return PxFilterFlag::eDEFAULT;
}

//helper function to set up filtering
void Physics::setupFiltering(PxRigidActor* actor, PxU32 filterGroup, PxU32 filterMask)
{
	PxFilterData filterData;
	filterData.word0 = filterGroup; // word0 = own ID.
	filterData.word1 = filterMask; //  word1 = ID mask to filter pairs that trigger a contact callback;
	const PxU32 numShapes = actor->getNbShapes();
	PxShape** shapes = (PxShape**)_aligned_malloc(sizeof(PxShape*)*numShapes, 16);
	actor->getShapes(shapes, numShapes);
	for (PxU32 i = 0; i < numShapes; i++)
	{
		PxShape* shape = shapes[i];
		shape->setSimulationFilterData(filterData);
	}
	_aligned_free(shapes);
}

void Physics::setShapeAsTrigger(PxRigidActor* actorIn)
{
	PxRigidStatic* staticActor = actorIn->is<PxRigidStatic>();
	assert(staticActor);
	const PxU32 numShapes = staticActor->getNbShapes();
	PxShape** shapes = (PxShape**)_aligned_malloc(sizeof(PxShape*)*numShapes, 16);
	staticActor->getShapes(shapes, numShapes);
	for (PxU32 i = 0; i < numShapes; i++)
	{
		shapes[i]->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
		shapes[i]->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);
	}
}