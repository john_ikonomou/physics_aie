#include "Box.h"



Box::Box() : RigidBody(ShapeType::DynamicBox)
{
	SetExtents(glm::vec3(0, 0, 0));
}

Box::Box(glm::vec3 Position, glm::vec3 Extents, float Mass, glm::vec4 Colour) : RigidBody(ShapeType::DynamicBox)
{
	SetPosition(Position);
	SetExtents(Extents);
	SetMass(Mass);
	SetDebugColour(Colour);
}

void Box::DebugDraw() const
{
	Gizmos::addAABBFilled(GetPosition(), GetExtents(), GetDebugColour());
}

StaticBox::StaticBox() : StaticObject(ShapeType::StaticBox)
{
	SetExtents(glm::vec3(0, 0, 0));
}

StaticBox::StaticBox(glm::vec3 Position, glm::vec3 Extents, glm::vec4 Colour) : StaticObject(ShapeType::StaticBox)
{
	SetPosition(Position);
	SetExtents(Extents);
	SetDebugColour(Colour);
}

void StaticBox::DebugDraw() const
{
	Gizmos::addAABB(GetPosition(), GetExtents(), GetDebugColour());
}
