#pragma once
#include "glm/ext.hpp"
#include "Gizmos.h"
#include "RigidBody.h"
#include "StaticObject.h"
class Sphere : public RigidBody
{
public:
	Sphere();
	Sphere(glm::vec3 Position, float Radius, float Mass, glm::vec4 Colour);

	float GetRadius() const { return m_Radius; }
	void SetRadius(float val) { m_Radius = val; }

	virtual void DebugDraw() const override;
private:
	float m_Radius;
};

class StaticSphere : public StaticObject
{
public:
	StaticSphere();
	StaticSphere(glm::vec3 Position, float Radius, glm::vec4 Colour);

	float GetRadius() const { return m_Radius; }
	void SetRadius(float val) { m_Radius = val; }

	virtual void DebugDraw() const override;
private:
	float m_Radius;
};

