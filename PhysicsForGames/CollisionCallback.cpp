#include "CollisionCallback.h"
#include "Physics.h"
#include <iostream>
using namespace std;

void CollisionCallback::onContact(const PxContactPairHeader& pairHeader, const PxContactPair* pairs, PxU32 nbPairs)
{
	for (PxU32 i = 0; i < nbPairs; i++)
	{
		const PxContactPair& cp = pairs[i];
		//only interested in touches found and lost
		if (cp.events & PxPairFlag::eNOTIFY_TOUCH_FOUND)
		{
			cout << "Collision Detected between: ";
			cout << pairHeader.actors[0]->getName();
			cout << pairHeader.actors[1]->getName() << endl;
		}
	}
}

void CollisionCallback::onTrigger(PxTriggerPair* pairs, PxU32 nbPairs)
{
	for (PxU32 i = 0; i < nbPairs; i++)
	{
		PxTriggerPair* pair = pairs + i;
		PxActor* triggerActor = pair->triggerActor;
		PxActor* otherActor = pair->otherActor;
		//Say some stuff
		if (otherActor->getName() != nullptr && triggerActor->getName() != nullptr)
		{
			cout << otherActor->getName();
			cout << " Entered Trigger ";
			cout << triggerActor->getName() << endl;

			Physics::s_isFluidActive = true;
		}
	}
};
