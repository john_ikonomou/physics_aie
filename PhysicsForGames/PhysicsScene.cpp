#include "PhysicsScene.h"
#include "PhysicsActor.h"

#include <iostream>
#include <thread>

#include "Sphere.h"
#include "Box.h"
#include "Plane.h"

PhysicsScene::PhysicsScene()
{
}


PhysicsScene::~PhysicsScene()
{
}

void PhysicsScene::AddActor(PhysicsActor * pActor)
{
	pActor->SetOwningScene(this);
	m_Actors.push_back(pActor);
}

void PhysicsScene::RemoveActor(PhysicsActor * pActor)
{
	for (int P = 0; P < m_Actors.size(); ++P)
	{		
		if (m_Actors[P] == pActor)
		{
			m_Actors.erase(m_Actors.begin() + P);
		}
	}
}

void PhysicsScene::RemoveAllActors()
{
	m_Actors.clear();
}

void PhysicsScene::Update(float DeltaTime)
{
	//This is some test multithreading code.
	/*
	//std::vector<std::thread> threads;
	//int threadsn = std::thread::hardware_concurrency();

	//if (m_Actors.size() < threadsn)
	//{
	//	threadsn = m_Actors.size();
	//}

	//for (int t = 0; t < threadsn; ++t)
	//{
	//		int chunklength = m_Actors.size() / threadsn;
	//		threads.push_back(std::thread([&](int low, int high)
	//		{
	//			for (int j = low; j < high; ++j)
	//			{
	//				m_Actors[j]->Update(DeltaTime);
	//			}
	//		}, t * chunklength + (m_Actors.size() % threadsn), (t + 1) * chunklength + (m_Actors.size() % threadsn)
	//		));
	//}
	//for (auto& thread : threads)
	//{
	//	thread.join();
	//}
	//threads.clear();
	*/

	//SINGLE THREAD
	for (int O = 0; O < m_Actors.size(); ++O)
	{
		m_Actors[O]->Update(DeltaTime);
	}

	CheckCollisions();
}

void PhysicsScene::DebugDraw() const
{
	for (int P = 0; P < m_Actors.size(); ++P)
	{
		m_Actors[P]->DebugDraw();
	}
}

typedef bool(*fn)(PhysicsActor*, PhysicsActor*);
static fn CollisionFunctionArray[] =
{
	PhysicsScene::Box2Box,			   PhysicsScene::Box2Sphere,		   PhysicsScene::Box2Plane,			  PhysicsScene::Box2StaticBox,			 PhysicsScene::Box2StaticSphere,
	PhysicsScene::Sphere2Box,		   PhysicsScene::Sphere2Sphere,		   PhysicsScene::Sphere2Plane,		  PhysicsScene::Sphere2StaticBox,		 PhysicsScene::Sphere2StaticSphere,
	PhysicsScene::Plane2Box,		   PhysicsScene::Plane2Sphere,		   PhysicsScene::Plane2Plane,		  PhysicsScene::Plane2StaticBox,		 PhysicsScene::Plane2StaticSphere,
	PhysicsScene::StaticBox2Box,	   PhysicsScene::StaticBox2Sphere,     PhysicsScene::StaticBox2Plane,     PhysicsScene::StaticBox2StaticBox,	 PhysicsScene::StaticBox2StaticSphere,
	PhysicsScene::StaticSphere2Box,    PhysicsScene::StaticSphere2Sphere,  PhysicsScene::StaticSphere2Plane,  PhysicsScene::StaticSphere2StaticBox,  PhysicsScene::StaticSphere2StaticSphere,
};

void PhysicsScene::CheckCollisions()
{
	unsigned int ActorCount = m_Actors.size();

	for (int outer = 0; outer < ActorCount - 1; ++outer)
	{
		for (int inner = outer + 1; inner < ActorCount; ++inner)
		{
			PhysicsActor* A = m_Actors[outer];
			int ShapeA = A->GetShapeID();

			PhysicsActor* B = m_Actors[inner];
			int ShapeB = B->GetShapeID();
			if (ShapeA != PhysicsActor::ShapeType::Spring && ShapeB != PhysicsActor::ShapeType::Spring)
			{
				int FunctionIndex = (ShapeA * NUMBERSHAPES) + ShapeB;
				fn CollisionFunctionPTR = CollisionFunctionArray[FunctionIndex];
				if (CollisionFunctionPTR != NULL)
				{
					if (CollisionFunctionPTR(A, B))
					{
						bool CollisionOccured = true;
					}
				}
			}
		}
	}
}

bool PhysicsScene::Box2Box(PhysicsActor * A, PhysicsActor * B)
{
	Box* bA = dynamic_cast<Box*>(A);
	Box* bB = dynamic_cast<Box*>(B);

	if (bA != NULL && bB != NULL)
	{
		//X Axis Collision Detection
		float X1 = bA->GetPosition().x;
		float Xmin1 = X1 - bA->GetExtents().x;
		float Xmax1 = X1 + bA->GetExtents().x;

		float X2 = bB->GetPosition().x;
		float Xmin2 = X2 - bB->GetExtents().x;
		float Xmax2 = X2 + bB->GetExtents().x;

		float sX1 = Xmax1 - Xmin2;
		float sX2 = Xmax2 - Xmin1;

		//Y Collision Detection
		float Y1 = bA->GetPosition().y;
		float Ymin1 = Y1 - bA->GetExtents().y;
		float Ymax1 = Y1 + bA->GetExtents().y;

		float Y2 = bB->GetPosition().y;
		float Ymin2 = Y2 - bB->GetExtents().y;
		float Ymax2 = Y2 + bB->GetExtents().y;

		float sY1 = Ymax1 - Ymin2;
		float sY2 = Ymax2 - Ymin1;

		//Z Collision Detection
		float Z1 = bA->GetPosition().z;
		float Zmin1 = Z1 - bA->GetExtents().z;
		float Zmax1 = Z1 + bA->GetExtents().z;

		float Z2 = bB->GetPosition().z;
		float Zmin2 = Z2 - bB->GetExtents().z;
		float Zmax2 = Z2 + bB->GetExtents().z;

		float sZ1 = Zmax1 - Zmin2;
		float sZ2 = Zmax2 - Zmin1;

		if (sX1 < 0 || sX2 < 0 || sY1 < 0 || sY2 < 0 || sZ1 < 0 || sZ2 < 0)
		{
			return false;
		}
		//X
		float xOverlap = 0;
		int xDirection = 0;
		if (sX1 < sX2)
		{
			xOverlap = sX1;
			xDirection = 1;
		}
		else
		{
			xOverlap = sX2;
			xDirection = -1;
		}
		//Y
		float yOverlap = 0;
		int yDirection = 0;
		if (abs(sY1) < abs(sY2))
		{
			yOverlap = sY1;
			yDirection = 1;
		}
		else
		{
			yOverlap = sY2;
			yDirection = -1;
		}
		//Z
		float zOverlap = 0;
		int zDirection = 0;
		if (abs(sZ1) < abs(sZ2))
		{
			zOverlap = sZ1;
			zDirection = 1;
		}
		else
		{
			zOverlap = sZ2;
			zDirection = -1;
		}
		//Get the smallest Distance
		float fInterceptDist;
		glm::vec3 vCollisionNormal;
		if (xOverlap < yOverlap)
		{
			if (xOverlap < zOverlap) { fInterceptDist = xOverlap, vCollisionNormal = glm::vec3(xDirection, 0, 0); }
			else { fInterceptDist = zOverlap, vCollisionNormal = glm::vec3(0, 0, zDirection); }
		}
		else
		{
			if (yOverlap < zOverlap) { fInterceptDist = yOverlap, vCollisionNormal = glm::vec3(0, yDirection, 0); }
			else { fInterceptDist = zOverlap, vCollisionNormal = glm::vec3(0, 0, zDirection); }

		}
		//Collision Detected - Now resolve
		glm::vec3 Direction = vCollisionNormal;

		glm::vec3 RelativeV = bA->GetVelocity() - bB->GetVelocity();

		glm::vec3 CollisionVector = Direction * (glm::dot(RelativeV, Direction));
		glm::vec3 ForceVector = CollisionVector * 1.0f / (1 / bA->GetMass() + 1 / bB->GetMass());
		bA->AddForceToActor(2 * ForceVector, bB, true);
		
		bA->SetPosition(bA->GetPosition() - (vCollisionNormal * fInterceptDist * 0.5f));
		bB->SetPosition(bB->GetPosition() + (vCollisionNormal * fInterceptDist * 0.5f));

		return true;
	}

	return false;
}

bool PhysicsScene::Box2Sphere(PhysicsActor * A, PhysicsActor * B)
{
	return Sphere2Box(B, A);
}

bool PhysicsScene::Box2Plane(PhysicsActor * A, PhysicsActor * B)
{
	//Box 2 Plane
	Box* bA = dynamic_cast<Box*>(A);
	Plane* pB = dynamic_cast<Plane*>(B);

	if (bA != NULL && pB != NULL)
	{
		glm::vec3 E = bA->GetExtents();
		glm::vec3 Normal = pB->GetDirection();

		float Radius = abs(Normal.x * E.x) + abs(Normal.y * E.y) + abs(Normal.z * E.z);
		Sphere S = Sphere(bA->GetPosition(), Radius, bA->GetMass(), glm::vec4(0));
		S.SetVelocity(bA->GetVelocity());

		if(S2P(&S, pB, bA))
		{
			return true;
		}
		return false;
	}
	return false;
}

bool PhysicsScene::S2P(PhysicsActor* A, PhysicsActor* B, PhysicsActor* C)
{
	Sphere* sphere = dynamic_cast<Sphere*>(A);
	Plane* plane = dynamic_cast<Plane*>(B);
	Box* b = dynamic_cast<Box*>(C);

	if (b != NULL && plane != NULL && sphere != NULL)
	{
		glm::vec3 CollisionNormal = plane->GetDirection();
		float SphereToPlane = glm::dot(sphere->GetPosition(), plane->GetDirection()) - plane->GetDistance();

		//if (SphereToPlane < 0)
		//{
		//	CollisionNormal *= -1;
		//	SphereToPlane *= -1;
		//}

		float intersection = sphere->GetRadius() - SphereToPlane;
		if (intersection > 0)
		{
			glm::vec3 PlaneNormal = plane->GetDirection();
			if (SphereToPlane < 0)
			{
				PlaneNormal *= -1;
			}

			glm::vec3 ForceVector = -1 * sphere->GetMass() * PlaneNormal * (glm::dot(PlaneNormal, sphere->GetVelocity()));
			b->AddForce(2 * ForceVector, true);

			b->SetPosition(b->GetPosition() + (CollisionNormal * (intersection * 0.5f)));
			return true;
		}
		return false;
	}
	return false;

}

bool PhysicsScene::Box2StaticBox(PhysicsActor * A, PhysicsActor * B)
{
	Box* bA = dynamic_cast<Box*>(A);
	StaticBox* bB = dynamic_cast<StaticBox*>(B);

	if (bA != NULL && bB != NULL)
	{
		//X Axis Collision Detection
		float X1 = bA->GetPosition().x;
		float Xmin1 = X1 - bA->GetExtents().x;
		float Xmax1 = X1 + bA->GetExtents().x;

		float X2 = bB->GetPosition().x;
		float Xmin2 = X2 - bB->GetExtents().x;
		float Xmax2 = X2 + bB->GetExtents().x;

		float sX1 = Xmax1 - Xmin2;
		float sX2 = Xmax2 - Xmin1;

		//Y Collision Detection
		float Y1 = bA->GetPosition().y;
		float Ymin1 = Y1 - bA->GetExtents().y;
		float Ymax1 = Y1 + bA->GetExtents().y;

		float Y2 = bB->GetPosition().y;
		float Ymin2 = Y2 - bB->GetExtents().y;
		float Ymax2 = Y2 + bB->GetExtents().y;

		float sY1 = Ymax1 - Ymin2;
		float sY2 = Ymax2 - Ymin1;

		//Z Collision Detection
		float Z1 = bA->GetPosition().z;
		float Zmin1 = Z1 - bA->GetExtents().z;
		float Zmax1 = Z1 + bA->GetExtents().z;

		float Z2 = bB->GetPosition().z;
		float Zmin2 = Z2 - bB->GetExtents().z;
		float Zmax2 = Z2 + bB->GetExtents().z;

		float sZ1 = Zmax1 - Zmin2;
		float sZ2 = Zmax2 - Zmin1;

		if (sX1 < 0 || sX2 < 0 || sY1 < 0 || sY2 < 0 || sZ1 < 0 || sZ2 < 0)
		{
			return false;
		}
		//X
		float xOverlap = 0;
		int xDirection = 0;
		if (sX1 < sX2)
		{
			xOverlap = sX1;
			xDirection = 1;
		}
		else
		{
			xOverlap = sX2;
			xDirection = -1;
		}
		//Y
		float yOverlap = 0;
		int yDirection = 0;
		if (abs(sY1) < abs(sY2))
		{
			yOverlap = sY1;
			yDirection = 1;
		}
		else
		{
			yOverlap = sY2;
			yDirection = -1;
		}
		//Z
		float zOverlap = 0;
		int zDirection = 0;
		if (abs(sZ1) < abs(sZ2))
		{
			zOverlap = sZ1;
			zDirection = 1;
		}
		else
		{
			zOverlap = sZ2;
			zDirection = -1;
		}
		//Get the smallest Distance
		float fInterceptDist;
		glm::vec3 vCollisionNormal;
		if (xOverlap < yOverlap)
		{
			if (xOverlap < zOverlap) { fInterceptDist = xOverlap, vCollisionNormal = glm::vec3(xDirection, 0, 0); }
			else { fInterceptDist = zOverlap, vCollisionNormal = glm::vec3(0, 0, zDirection); }
		}
		else
		{
			if (yOverlap < zOverlap) { fInterceptDist = yOverlap, vCollisionNormal = glm::vec3(0, yDirection, 0); }
			else { fInterceptDist = zOverlap, vCollisionNormal = glm::vec3(0, 0, zDirection); }

		}
		//Collision Detected - Now resolve
		glm::vec3 Direction = vCollisionNormal;

		glm::vec3 RelativeV = bA->GetVelocity() - glm::vec3(0);

		glm::vec3 CollisionVector = Direction * (glm::dot(RelativeV, Direction));
		glm::vec3 ForceVector = CollisionVector * 1.0f / (1 / bA->GetMass() + 1 / bB->GetMass());
		bA->AddForce(2 * ForceVector, true);

		bA->SetPosition(bA->GetPosition() - (vCollisionNormal * fInterceptDist));

		return true;
	}

	return false;
}

bool PhysicsScene::Box2StaticSphere(PhysicsActor * A, PhysicsActor * B)
{
	return StaticSphere2Box(B, A);
}


bool PhysicsScene::Sphere2Box(PhysicsActor * A, PhysicsActor * aB)
{
	Sphere* S = dynamic_cast<Sphere*>(A);
	Box* B = dynamic_cast<Box*>(aB);
	
	if (B != NULL && S != NULL)
	{

		glm::vec3 Right   = glm::vec3(1, 0, 0);
		glm::vec3 Up      = glm::vec3(0, 1, 0); 
		glm::vec3 Forward = glm::vec3(0, 0, 1);

		//Sphere 2 Box Collisions
		glm::vec3 vOffset = S->GetPosition() - B->GetPosition();

		float Cx = glm::dot(vOffset, Right);
		float Cy = glm::dot(vOffset, Up);
		float Cz = glm::dot(vOffset, Forward);

		glm::vec3 hExtent = B->GetExtents();

		if (abs(Cx) > hExtent.x ) //X
		{
			Cx = hExtent.x * (Cx / abs(Cx));
		}
		if (abs(Cy) > hExtent.y) //Y
		{
			Cy = hExtent.y * (Cy / abs(Cy));
		}
		if (abs(Cz) > hExtent.z) //Z
		{
			Cz = hExtent.z * (Cz / abs(Cz));
		}

		glm::vec3 CollisionPoint = glm::vec3(Cx, Cy, Cz) + B->GetPosition();
		glm::vec3 Direction = S->GetPosition() - CollisionPoint;

		float fOffset = glm::length(Direction);
		
		if (fOffset == 0)
		{
			float Xmin = B->GetPosition().x - B->GetExtents().x;
			float Xmax = B->GetPosition().x + B->GetExtents().x;
			float sXmin = Xmin - S->GetPosition().x;
			float sXmax = Xmax - S->GetPosition().x;
			float xLowest = abs(sXmin) < abs(sXmax) ? sXmin : sXmax;

			float Ymin = B->GetPosition().y - B->GetExtents().y;
			float Ymax = B->GetPosition().y + B->GetExtents().y;
			float sYmin = Ymin - S->GetPosition().y;
			float sYmax = Ymax - S->GetPosition().y;
			float yLowest = abs(sYmin) < abs(sYmax) ? sYmin : sYmax;

			float Zmin = B->GetPosition().z - B->GetExtents().z;
			float Zmax = B->GetPosition().z + B->GetExtents().z;
			float sZmin = Zmin - S->GetPosition().z;
			float sZmax = Zmax - S->GetPosition().z;
			float zLowest = abs(sZmin) < abs(sZmax) ? sZmin : sZmax;

			//Get the smallest Distance
			float fInterceptDist;
			glm::vec3 vCollisionNormal;
			if (xLowest < yLowest)
			{
				if (xLowest < zLowest) { fInterceptDist = xLowest, vCollisionNormal = glm::vec3(glm::normalize(xLowest), 0, 0); }
				else { fInterceptDist = zLowest, vCollisionNormal = glm::vec3(0, 0, glm::normalize(zLowest)); }
			}
			else
			{
				if (yLowest < zLowest) { fInterceptDist = yLowest, vCollisionNormal = glm::vec3(0, glm::normalize(yLowest), 0); }
				else { fInterceptDist = zLowest, vCollisionNormal = glm::vec3(0, 0, glm::normalize(zLowest)); }

			}
			//Collision Detected - Now resolve
			glm::vec3 Direction = vCollisionNormal;

			glm::vec3 RelativeV = S->GetVelocity() - B->GetVelocity();

			glm::vec3 CollisionVector = Direction * (glm::dot(RelativeV, Direction));
			glm::vec3 ForceVector = CollisionVector * 1.0f / (1 / S->GetMass() + 1 / B->GetMass());
			S->AddForceToActor(2 * ForceVector, B, true);

			S->SetPosition(S->GetPosition() - (vCollisionNormal * fInterceptDist * 0.5f));
			B->SetPosition(B->GetPosition() + (vCollisionNormal * fInterceptDist * 0.5f));

			return true;
		}

		glm::vec3 collisionNormal = glm::normalize(Direction);
		float intersect = S->GetRadius() - fOffset;
		if (intersect > 0)
		{
			//Handle Collisions
			glm::vec3 Direction = collisionNormal;

			glm::vec3 RelativeV = S->GetVelocity() - B->GetVelocity();

			glm::vec3 CollisionVector = Direction * (glm::dot(RelativeV, Direction));
			glm::vec3 ForceVector = CollisionVector * 1.0f / (1 / S->GetMass() + 1 / B->GetMass());
			S->AddForceToActor(2 * ForceVector, B, true);

			S->SetPosition(S->GetPosition() + (collisionNormal * intersect * 0.5f));
			B->SetPosition(B->GetPosition() - (collisionNormal * intersect * 0.5f));

			return true;
		}


		return false;
	}
	return false;

}

bool PhysicsScene::Sphere2Sphere(PhysicsActor * A, PhysicsActor * B)
{
	Sphere* sA = dynamic_cast<Sphere*>(A);
	Sphere* sB = dynamic_cast<Sphere*>(B);

	if (sA != NULL && sB != NULL)
	{
		//Get the overlap for collison resolution
		glm::vec3 Delta = sA->GetPosition() - sB->GetPosition();
		float Distance = glm::length(Delta);
		float CombinedRadius = sA->GetRadius() + sB->GetRadius();
		float Intersection = CombinedRadius - Distance;
		if (Intersection > 0)
		{
			assert(glm::length(Delta) > 0.0f);
			glm::vec3 CollisionNormal = glm::normalize(Delta);

			glm::vec3 RelativeV = sA->GetVelocity() - sB->GetVelocity();

			glm::vec3 CollisionVector = CollisionNormal * (glm::dot(RelativeV, CollisionNormal));
			glm::vec3 ForceVector = CollisionVector * 1.0f / (1 / sA->GetMass() + 1 / sB->GetMass());

			sA->AddForceToActor(ForceVector, sB, true);

			glm::vec3 SeperationVector = CollisionNormal * Intersection * .5f;

			sA->SetPosition(sA->GetPosition() + SeperationVector);
			sB->SetPosition(sB->GetPosition() - SeperationVector);
			return true;
		}

	}
	return false;
}

bool PhysicsScene::Sphere2Plane(PhysicsActor * A, PhysicsActor * B)
{
	Sphere* sphere = dynamic_cast<Sphere*>(A);
	Plane* plane = dynamic_cast<Plane*>(B);

	if (sphere != NULL && plane != NULL)
	{
		glm::vec3 CollisionNormal = plane->GetDirection();

		float SphereToPlane = glm::dot(sphere->GetPosition(), plane->GetDirection()) - plane->GetDistance();
		//THIS CODE IS FOR IF I WANT A 2-SIDED PLANE
		//if (SphereToPlane < 0)
		//{
		//	CollisionNormal *= -1;
		//	SphereToPlane   *= -1;
		//}
		float intersection = sphere->GetRadius() - SphereToPlane;
		if (intersection > 0)
		{
			glm::vec3 PlaneNormal = plane->GetDirection();
			if (SphereToPlane < 0)
			{
				PlaneNormal *= -1;
			}

			glm::vec3 ForceVector = -1 * sphere->GetMass() * PlaneNormal * (glm::dot(PlaneNormal, sphere->GetVelocity()));
			sphere->AddForce(2 * ForceVector, true);

			sphere->SetPosition(sphere->GetPosition() + (CollisionNormal * (intersection * 0.5f)));
			return true;

		}
		return false;
	}
	return false;
}

bool PhysicsScene::Sphere2StaticBox(PhysicsActor * A, PhysicsActor * aB)
{
	Sphere* S = dynamic_cast<Sphere*>(A);
	StaticBox* B = dynamic_cast<StaticBox*>(aB);

	if (B != NULL && S != NULL)
	{

		glm::vec3 Right = glm::vec3(1, 0, 0);
		glm::vec3 Up = glm::vec3(0, 1, 0);
		glm::vec3 Forward = glm::vec3(0, 0, 1);

		//Sphere 2 Box Collisions
		glm::vec3 vOffset = S->GetPosition() - B->GetPosition();

		float Cx = glm::dot(vOffset, Right);
		float Cy = glm::dot(vOffset, Up);
		float Cz = glm::dot(vOffset, Forward);

		glm::vec3 hExtent = B->GetExtents();

		if (abs(Cx) > hExtent.x) //X
		{
			Cx = hExtent.x * (Cx / abs(Cx));
		}
		if (abs(Cy) > hExtent.y) //Y
		{
			Cy = hExtent.y * (Cy / abs(Cy));
		}
		if (abs(Cz) > hExtent.z) //Z
		{
			Cz = hExtent.z * (Cz / abs(Cz));
		}

		glm::vec3 CollisionPoint = glm::vec3(Cx, Cy, Cz) + B->GetPosition();
		glm::vec3 Direction = S->GetPosition() - CollisionPoint;

		float fOffset = glm::length(Direction);

		if (fOffset == 0)
		{
			float Xmin = B->GetPosition().x - B->GetExtents().x;
			float Xmax = B->GetPosition().x + B->GetExtents().x;
			float sXmin = Xmin - S->GetPosition().x;
			float sXmax = Xmax - S->GetPosition().x;
			float xLowest = abs(sXmin) < abs(sXmax) ? sXmin : sXmax;

			float Ymin = B->GetPosition().y - B->GetExtents().y;
			float Ymax = B->GetPosition().y + B->GetExtents().y;
			float sYmin = Ymin - S->GetPosition().y;
			float sYmax = Ymax - S->GetPosition().y;
			float yLowest = abs(sYmin) < abs(sYmax) ? sYmin : sYmax;

			float Zmin = B->GetPosition().z - B->GetExtents().z;
			float Zmax = B->GetPosition().z + B->GetExtents().z;
			float sZmin = Zmin - S->GetPosition().z;
			float sZmax = Zmax - S->GetPosition().z;
			float zLowest = abs(sZmin) < abs(sZmax) ? sZmin : sZmax;

			//Get the smallest Distance
			float fInterceptDist;
			glm::vec3 vCollisionNormal;
			if (xLowest < yLowest)
			{
				if (xLowest < zLowest) { fInterceptDist = xLowest, vCollisionNormal = glm::vec3(glm::normalize(xLowest), 0, 0); }
				else { fInterceptDist = zLowest, vCollisionNormal = glm::vec3(0, 0, glm::normalize(zLowest)); }
			}
			else
			{
				if (yLowest < zLowest) { fInterceptDist = yLowest, vCollisionNormal = glm::vec3(0, glm::normalize(yLowest), 0); }
				else { fInterceptDist = zLowest, vCollisionNormal = glm::vec3(0, 0, glm::normalize(zLowest)); }

			}
			//Collision Detected - Now resolve
			glm::vec3 Direction = vCollisionNormal;

			glm::vec3 RelativeV = S->GetVelocity() - glm::vec3(0);

			glm::vec3 CollisionVector = Direction * (glm::dot(RelativeV, Direction));
			glm::vec3 ForceVector = CollisionVector * 1.0f / (1 / S->GetMass() + 1 / B->GetMass());
			S->AddForce(2 * ForceVector, true);

			S->SetPosition(S->GetPosition() - (vCollisionNormal * fInterceptDist));
			//B->SetPosition(B->GetPosition() + (vCollisionNormal * fInterceptDist * 0.5f));

			return true;
		}

		glm::vec3 collisionNormal = glm::normalize(Direction);
		float intersect = S->GetRadius() - fOffset;
		if (intersect > 0)
		{
			//Handle Collisions
			glm::vec3 Direction = collisionNormal;

			glm::vec3 RelativeV = S->GetVelocity() - glm::vec3(0);

			glm::vec3 CollisionVector = Direction * (glm::dot(RelativeV, Direction));
			glm::vec3 ForceVector = CollisionVector * 1.0f / (1 / S->GetMass() + 1 / B->GetMass());
			S->AddForce(2 * ForceVector, true);

			S->SetPosition(S->GetPosition() + (collisionNormal * intersect));
			//B->SetPosition(B->GetPosition() - (collisionNormal * intersect * 0.5f));

			return true;
		}


		return false;
	}
	return false;

}

bool PhysicsScene::Sphere2StaticSphere(PhysicsActor * A, PhysicsActor * B)
{
	Sphere* sA = dynamic_cast<Sphere*>(A);
	StaticSphere* sB = dynamic_cast<StaticSphere*>(B);

	if (sA != NULL && sB != NULL)
	{
		//Get the overlap for collison resolution
		glm::vec3 Delta = sA->GetPosition() - sB->GetPosition();
		float Distance = glm::length(Delta);
		float CombinedRadius = sA->GetRadius() + sB->GetRadius();
		float Intersection = CombinedRadius - Distance;
		if (Intersection > 0)
		{
			assert(glm::length(Delta) > 0.0f);
			glm::vec3 CollisionNormal = glm::normalize(Delta);

			glm::vec3 RelativeV = sA->GetVelocity() - glm::vec3(0);

			glm::vec3 CollisionVector = CollisionNormal * (glm::dot(RelativeV, CollisionNormal));
			glm::vec3 ForceVector = CollisionVector * 1.0f / (1 / sA->GetMass() + 1 / sB->GetMass());

			sA->AddForce(ForceVector, true);

			glm::vec3 SeperationVector = CollisionNormal * Intersection;

			sA->SetPosition(sA->GetPosition() + SeperationVector);
			//sB->SetPosition(sB->GetPosition() - SeperationVector);
			return true;
		}

	}
	return false;
}

bool PhysicsScene::Plane2Box(PhysicsActor * A, PhysicsActor * B)
{
	return Box2Plane(B, A);
}

bool PhysicsScene::Plane2Sphere(PhysicsActor * A, PhysicsActor * B)
{
	Sphere2Plane(B, A);
	return false;
}

bool PhysicsScene::Plane2Plane(PhysicsActor * A, PhysicsActor * B)
{
	return false;
}

bool PhysicsScene::Plane2StaticBox(PhysicsActor * A, PhysicsActor * B)
{
	return false;
}

bool PhysicsScene::Plane2StaticSphere(PhysicsActor * A, PhysicsActor * B)
{
	return false;
}

bool PhysicsScene::StaticBox2Box(PhysicsActor * A, PhysicsActor * B)
{
	return Box2StaticBox(B,A);
}

bool PhysicsScene::StaticBox2Sphere(PhysicsActor * A, PhysicsActor * B)
{
	return Sphere2StaticBox(B, A);
}

bool PhysicsScene::StaticBox2Plane(PhysicsActor * A, PhysicsActor * B)
{
	return false;
}

bool PhysicsScene::StaticBox2StaticBox(PhysicsActor * A, PhysicsActor * B)
{
	return false;
}

bool PhysicsScene::StaticBox2StaticSphere(PhysicsActor * A, PhysicsActor * B)
{
	return false;
}

bool PhysicsScene::StaticSphere2Box(PhysicsActor * A, PhysicsActor * aB)
{
	StaticSphere* S = dynamic_cast<StaticSphere*>(A);
	Box* B = dynamic_cast<Box*>(aB);

	if (B != NULL && S != NULL)
	{

		glm::vec3 Right = glm::vec3(1, 0, 0);
		glm::vec3 Up = glm::vec3(0, 1, 0);
		glm::vec3 Forward = glm::vec3(0, 0, 1);

		//Sphere 2 Box Collisions
		glm::vec3 vOffset = S->GetPosition() - B->GetPosition();

		float Cx = glm::dot(vOffset, Right);
		float Cy = glm::dot(vOffset, Up);
		float Cz = glm::dot(vOffset, Forward);

		glm::vec3 hExtent = B->GetExtents();

		if (abs(Cx) > hExtent.x) //X
		{
			Cx = hExtent.x * (Cx / abs(Cx));
		}
		if (abs(Cy) > hExtent.y) //Y
		{
			Cy = hExtent.y * (Cy / abs(Cy));
		}
		if (abs(Cz) > hExtent.z) //Z
		{
			Cz = hExtent.z * (Cz / abs(Cz));
		}

		glm::vec3 CollisionPoint = glm::vec3(Cx, Cy, Cz) + B->GetPosition();
		glm::vec3 Direction = S->GetPosition() - CollisionPoint;

		float fOffset = glm::length(Direction);

		if (fOffset == 0)
		{
			float Xmin = B->GetPosition().x - B->GetExtents().x;
			float Xmax = B->GetPosition().x + B->GetExtents().x;
			float sXmin = Xmin - S->GetPosition().x;
			float sXmax = Xmax - S->GetPosition().x;
			float xLowest = abs(sXmin) < abs(sXmax) ? sXmin : sXmax;

			float Ymin = B->GetPosition().y - B->GetExtents().y;
			float Ymax = B->GetPosition().y + B->GetExtents().y;
			float sYmin = Ymin - S->GetPosition().y;
			float sYmax = Ymax - S->GetPosition().y;
			float yLowest = abs(sYmin) < abs(sYmax) ? sYmin : sYmax;

			float Zmin = B->GetPosition().z - B->GetExtents().z;
			float Zmax = B->GetPosition().z + B->GetExtents().z;
			float sZmin = Zmin - S->GetPosition().z;
			float sZmax = Zmax - S->GetPosition().z;
			float zLowest = abs(sZmin) < abs(sZmax) ? sZmin : sZmax;

			//Get the smallest Distance
			float fInterceptDist;
			glm::vec3 vCollisionNormal;
			if (xLowest < yLowest)
			{
				if (xLowest < zLowest) { fInterceptDist = xLowest, vCollisionNormal = glm::vec3(glm::normalize(xLowest), 0, 0); }
				else { fInterceptDist = zLowest, vCollisionNormal = glm::vec3(0, 0, glm::normalize(zLowest)); }
			}
			else
			{
				if (yLowest < zLowest) { fInterceptDist = yLowest, vCollisionNormal = glm::vec3(0, glm::normalize(yLowest), 0); }
				else { fInterceptDist = zLowest, vCollisionNormal = glm::vec3(0, 0, glm::normalize(zLowest)); }

			}
			//Collision Detected - Now resolve
			glm::vec3 Direction = vCollisionNormal;

			glm::vec3 RelativeV = glm::vec3(0) - B->GetVelocity();

			glm::vec3 CollisionVector = Direction * (glm::dot(RelativeV, Direction));
			glm::vec3 ForceVector = CollisionVector * 1.0f / (1 / S->GetMass() + 1 / B->GetMass());
			B->AddForce(2 * ForceVector, true);

			//S->SetPosition(S->GetPosition() - (vCollisionNormal * fInterceptDist * 0.5f));
			B->SetPosition(B->GetPosition() + (vCollisionNormal * fInterceptDist * 0.5f));

			return true;
		}

		glm::vec3 collisionNormal = glm::normalize(Direction);
		float intersect = S->GetRadius() - fOffset;
		if (intersect > 0)
		{
			//Handle Collisions
			glm::vec3 Direction = collisionNormal;

			glm::vec3 RelativeV = glm::vec3(0) - B->GetVelocity();

			glm::vec3 CollisionVector = Direction * (glm::dot(RelativeV, Direction));
			glm::vec3 ForceVector = CollisionVector * 1.0f / (1 / S->GetMass() + 1 / B->GetMass());
			B->AddForce(2 * ForceVector, true);

			//S->SetPosition(S->GetPosition() + (collisionNormal * intersect * 0.5f));
			B->SetPosition(B->GetPosition() - (collisionNormal * intersect));

			return true;
		}


		return false;
	}
	return false;
}

bool PhysicsScene::StaticSphere2Sphere(PhysicsActor * A, PhysicsActor * B)
{
	return Sphere2StaticSphere(B, A);
}

bool PhysicsScene::StaticSphere2Plane(PhysicsActor * A, PhysicsActor * B)
{
	return false;
}

bool PhysicsScene::StaticSphere2StaticBox(PhysicsActor * A, PhysicsActor * B)
{
	return false;
}

bool PhysicsScene::StaticSphere2StaticSphere(PhysicsActor * A, PhysicsActor * B)
{
	return false;
}
