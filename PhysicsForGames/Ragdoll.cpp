#include "Ragdoll.h"

PxArticulation* Ragdoll::MakeRagdoll(PxPhysics * g_Physics, RagdollNode ** nodeArray, PxTransform worldPos, float scaleFactor, PxMaterial * ragdollMaterial)
{
	//create the articulation for our ragdoll
	PxArticulation *Articulation = g_Physics->createArticulation();
	RagdollNode** CurrentNode = nodeArray;
	//while there are more nodes to process...
	while (*CurrentNode != NULL)
	{
		//get a pointer to the current node
		RagdollNode* CurrentNodePtr = *CurrentNode;
		//create a pointer ready to hold the parent node pointer if there is one
		RagdollNode* parentNode = nullptr;
		//get scaled values for capsule
		float radius = CurrentNodePtr->radius * scaleFactor;
		float halfLength = CurrentNodePtr->halfLength * scaleFactor;
		float childHalfLength = radius + halfLength;
		float parentHalfLength = 0; //will be set later if there is a parent
									//get a pointer to the parent
		PxArticulationLink* parentLinkPtr = NULL;
		CurrentNodePtr->scaledGobalPos = worldPos.p;

		if (CurrentNodePtr->parentNodeIdx != -1)
		{
			//if there is a parent then we need to work out our local position for the link
			//get a pointer to the parent node
			parentNode = *(nodeArray + CurrentNodePtr->parentNodeIdx);

			//get a pointer to the link for the parent
			parentLinkPtr = parentNode->linkPtr;
			parentHalfLength = (parentNode->radius + parentNode->halfLength) *scaleFactor;

			//work out the local position of the node
			PxVec3 currentRelative = CurrentNodePtr->childLinkPos * CurrentNodePtr->globalRotation.rotate(PxVec3(childHalfLength, 0, 0));
			PxVec3 parentRelative = -CurrentNodePtr->parentLinkPos * parentNode->globalRotation.rotate(PxVec3(parentHalfLength, 0, 0));
			CurrentNodePtr->scaledGobalPos = parentNode->scaledGobalPos - (parentRelative + currentRelative);
		}

		//build the transform for the link
		PxTransform linkTransform = PxTransform(CurrentNodePtr->scaledGobalPos, CurrentNodePtr->globalRotation);
		//create the link in the articulation
		PxArticulationLink* link = Articulation->createLink(parentLinkPtr, linkTransform);
		//add the pointer to this link into the ragdoll data so we have it for later when we want to link to it
		CurrentNodePtr->linkPtr = link;
		float jointSpace = .01f; //gap between joints
		float capsuleHalfLength = (halfLength > jointSpace ? halfLength - jointSpace : 0) + .01f;
		PxCapsuleGeometry capsule(radius, capsuleHalfLength);
		link->createShape(capsule, *ragdollMaterial); //adds a capsule collider to the link
		PxRigidBodyExt::updateMassAndInertia(*link, 50.0f); //adds some mass, mass should really be part of the data!

		if (CurrentNodePtr->parentNodeIdx != -1)
		{
			//get the pointer to the joint from the link
			PxArticulationJoint *joint = link->getInboundJoint();
			//get the relative rotation of this link
			PxQuat frameRotation = parentNode->globalRotation.getConjugate() * CurrentNodePtr->globalRotation;
			//set the parent contraint frame
			PxTransform parentConstraintFrame = PxTransform(PxVec3(CurrentNodePtr->parentLinkPos * parentHalfLength, 0, 0), frameRotation);
			//set the child constraint frame (this the constraint frame of the newly added link)
			PxTransform thisConstraintFrame = PxTransform(PxVec3(CurrentNodePtr->childLinkPos * childHalfLength, 0, 0));
			//set up the poses for the joint so it is in the correct place
			joint->setParentPose(parentConstraintFrame);
			joint->setChildPose(thisConstraintFrame);
			//set up some constraints to stop it flopping around
			joint->setStiffness(20);
			joint->setDamping(20);
			joint->setSwingLimit(0.4f, 0.4f);
			joint->setSwingLimitEnabled(true);
			joint->setTwistLimit(-0.1f, 0.1f);
			joint->setTwistLimitEnabled(true);
		}

		//get a pointer to the current node
		CurrentNode++;
	}
	return Articulation;
}

Ragdoll::Ragdoll()
{
}


Ragdoll::~Ragdoll()
{
}
