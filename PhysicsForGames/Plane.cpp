#include "Plane.h"
#include "Gizmos.h"

Plane::Plane() : StaticObject(ShapeType::StaticPlane)
{

}

void Plane::DebugDraw() const
{
	int InfiniteExtents = 32;
	glm::vec3 Z;
	glm::vec3 X;

	float Res = glm::dot(GetDirection(), glm::vec3(0,1,0));
	if (Res == 1 || Res == -1)
	{
		Z = glm::cross(GetDirection(), glm::vec3(1, 0, 0));
	}
	else
	{
		Z = glm::cross(GetDirection(), glm::vec3(0, 1, 0));
	}
	X = glm::cross(GetDirection(), Z);
	glm::mat4 Rotation = glm::mat4(0);
	Rotation[0] = glm::vec4(X, 0);
	Rotation[1] = glm::vec4(GetDirection(), 0);
	Rotation[2] = glm::vec4(Z, 0);

	glm::vec3 PlanePosition = GetDirection() * GetDistance();
	Gizmos::addAABBFilled(PlanePosition, glm::vec3(InfiniteExtents, 0.0005f, InfiniteExtents), GetDebugColour(), &Rotation);
	Rotation[3] = glm::vec4(PlanePosition,1);
	Gizmos::addTransform(Rotation);
}
