#pragma once
#include "PhysicsActor.h"
class Spring : public PhysicsActor
{
public:
	Spring();
	Spring(float a_Damping, float a_SpringCo);
	~Spring();

	void Attach(PhysicsActor* A, PhysicsActor* B);
	virtual void Update(float deltaTime) override;
	virtual void DebugDraw() const override;

	float GetDamping() const { return m_Damping; }
	void SetDamping(float val) { m_Damping = val; }

	float GetSpringConstant() const { return m_SpringConstant; }
	void SetSpringConstant(float val) { m_SpringConstant = val; }

	float GetRestLength() const { return RestLength; }
	void SetRestLength(float val) { RestLength = val; }

	PhysicsActor* Start;
	PhysicsActor* End;
private:
	float m_Damping;
	float m_SpringConstant;
	float RestLength;
};

