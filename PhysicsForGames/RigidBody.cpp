#include "RigidBody.h"
#include "PhysicsScene.h"

#include <iostream>

RigidBody::RigidBody(PhysicsActor::ShapeType eType) : PhysicsActor(eType)
{
	ClearForces();
	m_pInfo = PhysicsInfo(1,1,FLT_MAX);
}

void RigidBody::Update(float fDeltaTime)
{
	if (glm::length(GetVelocity()) < 0.008f)
	{
		SetVelocity(glm::vec3(0));
	}

	glm::vec3 Gravity = glm::vec3(0);
	Gravity = GetOwningScene()->GetGravity();
	//Zero out Acceleration
	glm::vec3 Acceleration = glm::vec3(0);
	//Set acceleration based on forces and gravity.
	Acceleration = (m_Forces / GetMass());
	SetAcceleration(Acceleration + Gravity);
	//Set velocity based on Acceleration
	SetVelocity(GetVelocity() + (GetAcceleration() * fDeltaTime));
	//Calculate Momentum
	SetMomentum(GetMass() * GetVelocity());
	//Set the position based on velocity
	SetPosition(GetPosition() + (GetVelocity() * fDeltaTime));
	//Simulate Drag
	SetVelocity(GetVelocity() * GetPhysicsInfo().Drag);

	ClearForces();
}

void RigidBody::AddForce(const glm::vec3 vForce, bool isImpulse)
{
	if(!isImpulse)
		m_Forces += vForce;
	else
	{
		m_Velocity += vForce / GetMass();
	}
}

void RigidBody::AddForceToActor(glm::vec3 vForce, RigidBody * aActor, bool isImpulse)
{
	AddForce(-vForce, isImpulse);
	aActor->AddForce(vForce, isImpulse);
}

void RigidBody::ClearForces()
{
	m_Forces = glm::vec3(0);
}

glm::vec3 RigidBody::SimulateIntegrationEuler(float Time, glm::vec3 iVel, glm::vec3 iPos)
{
	float yGrav = GetOwningScene()->GetGravity().y;
	float x = iPos.x + (Time * iVel.x);
	float z = iPos.z + (Time * iVel.z);
	float y = ((0.5f * yGrav)*Time*Time) + Time * iVel.y + iPos.y;

	return glm::vec3(x,y,z);
}
