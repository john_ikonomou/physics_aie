#pragma once
#include "RigidBody.h"
#include "StaticObject.h"
#include "Gizmos.h"
#include "glm/ext.hpp"

class Box : public RigidBody
{
public:
	Box();
	Box(glm::vec3 Position, glm::vec3 Extents, float Mass, glm::vec4 Colour);
	
	glm::vec3 GetExtents() const { return m_Extents; }
	void SetExtents(glm::vec3 val) { m_Extents = val; }

	virtual void DebugDraw() const override;
private:
	glm::vec3 m_Extents;
};

class StaticBox : public StaticObject
{
public:
	StaticBox();
	StaticBox(glm::vec3 Position, glm::vec3 Extents, glm::vec4 Colour);

	glm::vec3 GetExtents() const { return m_Extents; }
	void SetExtents(glm::vec3 val) { m_Extents = val; }

	virtual void DebugDraw() const override;
private:
	glm::vec3 m_Extents;
};

