#include <PxPhysicsAPI.h>
#include <PxScene.h>
using namespace physx;
#pragma once
class CollisionCallback : public PxSimulationEventCallback
{
public:
	bool Triggered = false;

	virtual void onContact(const PxContactPairHeader& pairHeader, const PxContactPair* pairs, PxU32 nbPairs);

	virtual void onTrigger(PxTriggerPair* pairs, PxU32 nbPairs);

	virtual void onConstraintBreak(PxConstraintInfo*, PxU32) 
	{

	};
	virtual void onWake(PxActor**, PxU32) 
	{

	};
	virtual void onSleep(PxActor**, PxU32) 	{	};
};

